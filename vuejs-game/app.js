new Vue({
  el: '#app',
  data: {
    playerHealth: 10,
    monsterHealth: 10,
    specialAttacks: 1,
    logs: [{message:"Welcome to Game Of Games",type:"info"}],
    gameRunning: false
  },
  methods: { 
    giveUp: function(){
      this.gameRunning = false;
      this.playerHealth = 10;
      this.monsterHealth = 10;
      this.specialAttacks = 1;
      this.addLog("game over", "info");
    },
    newGame: function(){
      this.gameRunning = !this.gameRunning;
    },
    attack: function(){
      this.monsterHealth--; 
      this.addLog("Damage to monster 1 point","success");
      this.monsterAttack();
    },
    specialAttack: function(){
      this.monsterHealth -= 3;
      this.specialAttacks--;
      this.addLog("Damage to monster 3 points","success");
      this.monsterAttack();
    },
    heal: function(){
      this.playerHealth = 10;
      this.addLog("You heal 10 points");
      this.monsterAttack();
    },
    monsterAttack: function(){
      if (this.monsterHealth <= 0){
        this.addLog("congratulations, you win! ", "success");
        this.giveUp();
        return;
      }
      this.playerHealth -= 3;
      if (this.playerHealth <= 0){
        this.addLog("BOO! you lose! ", "danger");
        this.giveUp();
        return;
      }
      this.addLog("Monster takes 3 points", "danger");
    },
    addLog: function(message, type="info"){
      // type = info, danger, success
      logs = this.logs;
      // trim logs if they are long (max 5 lines)
      max = 5;
      if (logs.length > max)
        logs.pop();
      logs.unshift({
        type:type,
        message:message,
      });
    }
  }
});
