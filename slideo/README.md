# Slideo - A Video Summary Out Of Any Page

 This tool is built on top of phaser.js 3 and is used to create a video from images and text that summarize the page content you are on.

 The real hard part is up to you which is to summarize the content.

 This tool autoloads by itself, all you need to do is add this container in the html:

```
 <div id="slideo"></div>
```

 and include the plugin file and before that set global params as such:


```
 <script>
 	// This is the style of the rendered text
    let style = {
      fontSize: '22px',
      color: '#fff',
      fontFamily: 'Montserrat',
      stroke: '#333',
      strokeThickness: 2.5,
      align: "center",
    };
    // array of slides
    const SLIDES = [
      {
        imageName: 'phaser',
        imageUrl:'img/phaser.png',
        text: 'first paragraph summary would go here then:',
        style: style
      },
      {
        imageName: 'hand',
        imageUrl:'img/hand.jpeg',
        text: 'second paragraph summary',
        style: style
      },
      {
        imageName: 'light_bulb',
        imageUrl:'img/light_bulb.jpg',
        text: 'second paragraph summary',
        style: style
      }
    ];

  const SLIDE_LOGO = {
    imageUrl: 'img/open.jpeg',
  };

  </script>
  <script src="js/phaser.js" type="text/javascript"></script>
  <script src="js/main.js" type="text/javascript"></script>
```

 If you would like to see an example you can either go here for the demo: https://cyber778.bitbucket.io/slideo/

or if you have python installed all you need todo is run `python server.py` and go to `localhost:8080`
